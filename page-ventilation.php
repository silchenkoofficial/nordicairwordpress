<?php 
/*
Template Name: Вентиляция
*/
?>

<?php get_header('ventilation'); ?>

    <section id="search" class="search">
        <div class="container">
            <div class="row">
                <a onclick="history.back()" class="arrow-back" style="cursor: pointer;">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/arrow-back.png" alt="back">
                    <p>Назад</p>
                </a>
            </div>
            <div class="row">
                <div class="d1">
                    <form>
                        <input type="text" placeholder="Найти товар...">
                        <button type="submit">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/search-icon.png" alt="Search">
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </section>


    <section id="ventilation" class="ventilation-main">
        <div class="ventilation__title">
            <h1>Вентиляционные установки</h1>
        </div>
        <div class="ventilation">
            <div class="container">
                <div class="ventilation__block">
                    <div class="ventilation__block--card card-1">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/ventilation-1.png" alt="">
                        <div class="ventilation__block--card--title">
                            <div class="ventilation__block--card--title--desc">
                                <h2>Приточно-вытяжная вентиляция</h2>
                                <p>Самый надежный способ очищения воздуха в доме!</p>
                            </div>
                            <a href="catalog">В каталог</a>
                        </div>
                    </div>
                    <div class="ventilation__block--card card-2">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/ventilation-2.png" alt="">
                        <div class="ventilation__block--card--title">
                            <div class="ventilation__block--card--title--desc">
                                <h2>Рекуператоры</h2>
                                <p>
                                    На улицу выходит полностью «отработанный» воздух, а в помещение попадает не
                                    только
                                    свежий, но и уже нагретый воздух.
                                </p>
                            </div>
                            <a href="catalog">В каталог</a>
                        </div>
                    </div>
                    <div class="ventilation__block--card card-3">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/ventilation-3.png" alt="">
                        <div class="ventilation__block--card--title">
                            <div class="ventilation__block--card--title--desc">
                                <h2>Бризеры</h2>
                                <p>
                                    Компактная приточная вентиляция с подогревом, очисткой воздуха и умным
                                    управлением
                                    со смартфона.
                                </p>
                            </div>
                            <a href="catalog">В каталог</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php get_footer('ventilation'); ?>