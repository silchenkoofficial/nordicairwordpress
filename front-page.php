<?php 
/*
Template Name: Главная
*/
?>

<?php get_header(); ?>
<section id="search" class="search">
        <div class="container">
            <div class="row">
                <div class="d1">
                    <form>
                        <input type="text" placeholder="Найти товар...">
                        <button type="submit">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/search-icon.png" alt="Search">
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section id="navigation" class="navigation">
        <div class="container">
            <div class="row">
                <div class="navigation__buttons">
                    <a href="home/conditioner" class="navigation__buttons--button">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/conditioner-1.png" alt="Conditioner 1">
                        <p>Кондиционирование</p>
                    </a>
                    <a href="home/ventilation" class="navigation__buttons--button">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/conditioning-2.png" alt="Conditioner 2">
                        <p>Вентиляция</p>
                    </a>
                    <a href="home/heating" class="navigation__buttons--button">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/conditioner-3.png" alt="Conditioner 3">
                        <p>Отопление</p>
                    </a>
                </div>
                <div class="col-3 mp0">
                    <div class="navigation__contact">
                        <div class="navigation__contact--operating-mode">
                            <p>Режим работы</p>
                            <h2>Пн-Пт</h2>
                            <h2>9:00-20:00</h2>
                        </div>
                        <div class="navigation__contact--tel">
                            <p>Телефон</p>
                            <h2>+7 (911) 755-93-76</h2>
                        </div>
                        <div class="navigation__contact--button">
                            <a href="tel:89117559376">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/phone.png" alt="Phone">
                                Связаться с нами
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="ad" class="ad">
        <div id="leftArrow" class="slider-arrow left" onclick="prevSlide()"></div>
        <div class="container">
            <div class="row">
                <div class="ad__slider">
                    <div class="item">
                        <div class="col">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/ad.png" alt="AD">
                        </div>
                        <div class="col">
                            <div class="ad__slider--description">
                                <p>Акция</p>
                                <h1>ПРИ ПОКУПКЕ КОНДИЦИОНЕРА DAIKIN – УСТАНОВКА В ПОДАРОК</h1>
                                <p>Акция распространяется на модели *модель1, *модель2</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="col">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/ad.png" alt="AD">
                        </div>
                        <div class="col">
                            <div class="ad__slider--description">
                                <p>Акция</p>
                                <h1>ПРИ ПОКУПКЕ КОНДИЦИОНЕРА DAIKIN – УСТАНОВКА В ПОДАРОК</h1>
                                <p>Акция распространяется на модели *модель1, *модель2</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="col">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/ad.png" alt="AD">
                        </div>
                        <div class="col">
                            <div class="ad__slider--description">
                                <p>Акция</p>
                                <h1>ПРИ ПОКУПКЕ КОНДИЦИОНЕРА DAIKIN – УСТАНОВКА В ПОДАРОК</h1>
                                <p>Акция распространяется на модели *модель1, *модель2</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="col">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/ad.png" alt="AD">
                        </div>
                        <div class="col">
                            <div class="ad__slider--description">
                                <p>Акция</p>
                                <h1>ПРИ ПОКУПКЕ КОНДИЦИОНЕРА DAIKIN – УСТАНОВКА В ПОДАРОК</h1>
                                <p>Акция распространяется на модели *модель1, *модель2</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ad__slider-dots">
                    <span class="ad__slider-dots--item" onclick="currentSlide(1)"></span>
                    <span class="ad__slider-dots--item" onclick="currentSlide(2)"></span>
                    <span class="ad__slider-dots--item" onclick="currentSlide(3)"></span>
                    <span class="ad__slider-dots--item" onclick="currentSlide(4)"></span>
                </div>
            </div>
        </div>
        <div id="rightArrow" class="slider-arrow right" onclick="nextSlide()"></div>
    </section>

    <section id="workStages" class="workStages">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="workStages__title">
                        <h1>Этапы нашей работы</h1>
                    </div>
                    <div class="workStages__description">
                        <div class="workStages__description--block">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/stage-1.png" alt="img">
                            <h2>Заказ товара</h2>
                            <p>Выберите товар, положите его в корзину и оформите заказ</p>
                        </div>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/stage-arrow.png" alt="arrow">
                        <div class="workStages__description--block">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/stage-2.png" alt="img">
                            <h2>Выезд на осмотр</h2>
                            <p>Наш менеджер свяжется с вами и договорится о дате осмотра</p>
                        </div>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/stage-arrow.png" alt="arrow">
                        <div class="workStages__description--block">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/stage-3.png" alt="img">
                            <h2>Доставка и установка</h2>
                            <p>Как только ваш заказ будет готов, мы его доставим и установим</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="contact-form" class="contact-form">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="contact-form__title">
                        <h1>Мы обязательно вам перезвоним</h1>
                        <p>Мы свяжемся с вами в ближайшее время и проконсультируем по всем интересующим вопросам!</p>
                    </div>
                </div>
                <div class="col">
                    <div id="invisible-form">
                        <form class="contact-form__form">
                            <div class="contact-form__form--input-block">
                                <p>Задайте вопрос</p>
                                <textarea id="question-input" name="question" cols="60" rows="5"></textarea>
                            </div>
                            <div class="contact-form__form--input-block">
                                <p>Номер телефона</p>
                                <input id="telephone-input" class="input-2" type="tel">
                            </div>
                            <div class="contact-form__form--input-block">
                                <p>Ваш Email</p>
                                <input id="email-input" class="input-3" type="email">
                            </div>
                            <div id="form-submit-btn" class="contact-form__form--submit"><span>Отправить</span></div>
                        </form>
                    </div>
                    <div id="send-block" class="invisible-send" style="display: none;">
                        <div class="contact-form__send">
                            <span>Ваш вопрос отправлен!</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="top-btn">
        <a href="#header">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/arrow-top.png" alt="arrow-top">
            В начало
        </a>
    </div>
<?php get_footer(); ?>