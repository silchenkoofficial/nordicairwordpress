<?php 
/*
Template Name: Страница товара
*/
?>

<?php get_header('product-page'); ?>

<section id="search" class="search">
        <div class="container">
            <div class="row">
                <a onclick="history.back()" class="arrow-back" style="cursor: pointer;">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/arrow-back.png" alt="back">
                    <p>Назад</p>
                </a>
            </div>
            <div class="row">
                <div class="d1">
                    <form>
                        <input type="text" placeholder="Найти товар...">
                        <button type="submit">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/search-icon.png" alt="Search">
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </section>

<section id="product" class="product">
        <div class="container">
            <div class="product__title">
                <div class="product__title--photo">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/product-photo.png" alt="Conditioner">
                </div>
                <div class="product__title--block">
                    <div class="product__title--head">
                        <h1>Кондиционер Aero<br>ALRS-II-09IHA4-01/ALRS-II-09OHA4-01</h1>
                    </div>
                    <div class="product__title--desc">
                        <div class="product__title--desc--property">
                            <p>Основные характеристики</p>
                            <div class="property__block">
                                <div class="brend">
                                    <span class="key">Производитель:</span>
                                    <span class="dotted"></span>
                                    <span class="value">Aero</span>
                                </div>
                                <div class="model">
                                    <span class="key">Модель:</span>
                                    <span class="dotted"></span>
                                    <span class="value">AeroLite</span>
                                </div>
                                <div class="country">
                                    <span class="key">Страна сборки:</span>
                                    <span class="dotted"></span>
                                    <span class="value">Китай</span>
                                </div>
                                <div class="square">
                                    <span class="key">Площадь помещения:</span>
                                    <span class="dotted"></span>
                                    <span class="value">25 кв.м.</span>
                                </div>
                                <div class="mode">
                                    <span class="key">Режим работы:</span>
                                    <span class="dotted"></span>
                                    <span class="value">Охлаждение и обогрев</span>
                                </div>
                                <div class="guarantee">
                                    <span class="key">Гарантийный срок:</span>
                                    <span class="dotted"></span>
                                    <span class="value">3 года</span>
                                </div>
                            </div>
                        </div>
                        <div class="product__title--desc--choice">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/people-choice.png" alt="">
                        </div>
                    </div>
                    <div class="product__title--price">
                        <p>10 000 ₽</p>
                        <span>+ Бесплатная доставка</span>
                    </div>
                    <span class="availability">
                        <div class="item"></div>
                        <p>В наличии</p>
                    </span>
                    <div class="add-to-cart">
                        <div class="cart-button">
                            <span class="cart-icon"></span>
                            <span class="cart-text">Добавить в корзину</span>
                        </div>
                        <div class="payment">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/payment.png" alt="Payment">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="product__description--buttons">
            <div class="button-description btn-active" id="button-description">
                <span>Описание</span>
            </div>
            <div class="button-property" id="button-property">
                <span>Характеристики товара</span>
            </div>
        </div>
        <div class="product__description">
            <div class="product__description--title">
                <div class="product__description--left">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/conditioner-photo.png" alt="Conditioner">
                </div>
                <div class="product__description--right">
                    <div class="product__description--block">
                        <h1>Настенная сплит-система Aero ALRS-II-09IHA4-01/ALRS-II-09OHA4-01</h1>
                        <p>
                            это обновленная линейка кондиционеров предыдущего поколения, которые хорошо себя зарекомендовали в 2016 году. Новые
                            AeroLITE с улучшенными техническими и функциональными характеристиками, отличаются простотой и удобством исполнения
                            сплит системы, надежностью и доступной ценой.
                        </p>
                        <p>
                            Элегантный внутренний блок с классом энергоэффективности - «A» и оптимальный набор функций создают идеальный баланс
                            дизайна и современных технологий. Внутренний блок оснащен скрытым дисплеем. Дисплей расположен на панели внутреннего
                            блока, может быть включен или отключен при помощи пульта ДУ.
                        </p>
                        <p>
                            Для очистки воздуха в корпусе внутреннего блока установлен фильтр холодного катализа, который отлично очищает воздух
                            от
                            вредных примесей.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="product__description2" style="display: none;">
            <div class="product__description2--title">
                <div class="title__property">
                    <h1>Характеристики</h1>
                    <div class="property__desc">
                        <div class="property__desc--block">
                            <div class="type">
                                <span class="key">Тип кондиционера:</span>
                                <span class="dotted"></span>
                                <span class="value">Настенная сплит-система</span>
                            </div>
                            <div class="square">
                                <span class="key">Обслуживаемая площадь:</span>
                                <span class="dotted"></span>
                                <span class="value">18 кв.м.</span>
                            </div>
                            <div class="invertor">
                                <span class="key">Инвертор (плавная регулировка мощности):</span>
                                <span class="dotted"></span>
                                <span class="value">есть</span>
                            </div>
                            <div class="class">
                                <span class="key">Класс энергопотребления:</span>
                                <span class="dotted"></span>
                                <span class="value">А</span>
                            </div>
                            <div class="mode">
                                <span class="key">Основные режимы:</span>
                                <span class="dotted"></span>
                                <span class="value">охлаждение / обогрев</span>
                            </div>
                            <div class="range">
                                <span class="key">Диапазон поддерживаемых температур:</span>
                                <span class="dotted"></span>
                                <span class="value">16 - 32°С</span>
                            </div>
                            <div class="power-in-cold">
                                <span class="key">Мощность в режиме охлаждения:</span>
                                <span class="dotted"></span>
                                <span class="value">2650 Вт</span>
                            </div>
                            <div class="power-in-hot">
                                <span class="key">Мощность в режиме обогрева:</span>
                                <span class="dotted"></span>
                                <span class="value">2700 Вт</span>
                            </div>
                            <div class="power-input-in-cold">
                                <span class="key">Потребляемая мощность при обогреве:</span>
                                <span class="dotted"></span>
                                <span class="value">747 Вт</span>
                            </div>
                            <div class="power-input-in-hot">
                                <span class="key">Потребляемая мощность при охлаждении:</span>
                                <span class="dotted"></span>
                                <span class="value">825 Вт</span>
                            </div>
                            <div class="drainage">
                                <span class="key">Режим осушения:</span>
                                <span class="dotted"></span>
                                <span class="value">есть</span>
                            </div>
                            <div class="controller">
                                <span class="key">Пульт дистанционного управления:</span>
                                <span class="dotted"></span>
                                <span class="value">есть</span>
                            </div>
                            <div class="timer">
                                <span class="key">Таймер включения/выключения:</span>
                                <span class="dotted"></span>
                                <span class="value">есть</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="title__special">
                    <h1>Особенности</h1>
                    <div class="special__desc">
                        <div class="special__desc--text">
                            <h2>Режим тихой работы</h2>
                            <p>
                                Обеспечит снижение уровня шумя, тем самым создаёт оптимальные условия для Вашего отдыха.
                            </p>
                        </div>
                        <div class="special__desc--text">
                            <h2>Режим сна</h2>
                            <p>
                                В этом режиме сплит система автоматически управляет температурой воздуха в помещении, делая Ваш сон максимально комфортным.
                            </p>
                        </div>
                        <div class="special__desc--text">
                            <h2>Функция автоматического перезапуска</h2>
                            <p>
                                В случае отключения электричества, сплит система автоматически перезапустится после восстановления питания, с параметрами сохраненными в памяти.
                            </p>
                        </div>
                        <div class="special__desc--text">
                            <h2>Функция самодиагностики</h2>
                            <p>
                                Позволяет отображать коды неисправностей на дисплее внутреннего блока, тем самым сокращая время на поиски и устранение неисправности.
                            </p>
                        </div>
                        <div class="special__desc--text">
                            <h2>Функция тёплый старт</h2>
                            <p>
                                Данная функция блокирует вентилятор внутреннего блока до тех пор, пока теплообменник не прогреется, поэтому в Вашу комнату будут поступать потоки только теплого воздуха.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="product__favorite">
            <div class="product__favorite--title">
                <h1>Также вам может понравиться:</h1>
            </div>
            <div class="wrapper">
                <?php getCards(5, stock); ?>
            </div>
        </div>
    </section>

<?php get_footer('product-page'); ?>