<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=1440">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>NordicAir</title>

    <!-- FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800,900&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/ventilation.css">

    <?php wp_head(); ?>

</head>
<body>
    <header id="header" class="header">
        <div class="container">
            <div class="row dfjcsb">
                <div class="col-4 d-flex" style="align-items: center;">
                        <?php echo the_custom_logo(); ?>
                    <a href="<?php bloginfo("url"); ?>" class="header__logo">
                        <div class="header__title">
                            <h1><?php bloginfo("name"); ?></h1>
                            <p><?php bloginfo("description"); ?></p>
                        </div>
                    </a>
                </div>
                <div class="col-8 navbar">
                    <nav class="header__menu">
                        <a id="catalog" class="header__menu--link" style="cursor: pointer;">Каталог</a>
                        <div class="dropdown--menu" id="dropdown--menu">
                            <div class="close-window"></div>
                            <ul>
                                <li><a href="catalog">Настенные сплит-системы</a></li>
                                <li><a href="javascript://">Мульти сплит-системы</a></li>
                                <li><a href="javascript://">Полупромышленные сплит-системы</a></li>
                                <li><a class="page-arrow" href="javascript://">Вентиляция</a></li>
                                <li><a class="page-arrow" href="javascript://">Отопление</a></li>
                            </ul>
                        </div>
                        <a href="services" class="header__menu--link">Услуги</a>
                        <a href="javascript://" class="header__menu--link">Наши работы</a>
                        <a href="about" class="header__menu--link">О нас</a>
                        <a href="javascript://" class="header__menu--link">Контакты</a>
                        <a href="javascript://" class="header__menu--cart"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/shopping-cart.png" alt="Корзина">Корзина</a>
                    </nav>
                </div>
            </div>
        </div>
    </header>