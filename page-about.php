<?php 
/*
Template Name: О нас
*/
?>

<?php get_header('about'); ?>

<section id="search" class="search">
    <div class="container">
        <div class="row">
            <a onclick="history.back()" class="arrow-back" style="cursor: pointer;">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/arrow-back.png" alt="back">
                <p>Назад</p>
            </a>
        </div>
    </div>
</section>

<section class="about" id="about">
    <div class="container">
        <h1>О нас</h1>
    </div>
    <div class="about__title">
        <div class="container">
            <div class="about__title--desc">
                <h2>Компания "NordicAir"</h2>
                <p>
                    Была основана в 2016 году. За это время было реализовано большое количество объектов по вентиляции и кондиционированию, начиная от квартир и заканчивая большими производственными помещениями.
                </p>
                <div class="about__links">
                    <a href="javascript://">Наши контакты</a>
                    <a href="javascript://">Примеры работы</a>
                </div>
            </div>
        </div>
    </div>
    <div class="about__list">
        <div class="about__list--item">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-ventilation.png" alt="">
            <div class="about__list--title">
                <h1>ТОЛЬКО ЛУЧШЕЕ ОБОРУДОВАНИЕ</h1>
                <p>
                    Мы предлагаем только качественную и проверенную технику от ведущих мировых производителей - Mitsubishi, Daikin, Toshiba, General, Haier, Hisense и прочее. Нам не выгодно продавать не качественную технику, так как при продаже мы берем на себя полную ответственность за ее работоспособность.
                </p>
            </div>
        </div>
        <div class="about__list--item">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-ventilation.png" alt="">
            <div class="about__list--title">
                <h1>МОНТАЖ ЛЮБОЙ СЛОЖНОСТИ</h1>
                <p>
                    За годы существования компании был накоплен огромный опыт по выполнению работ всех видов работ от простого к сложному. Опытный сотрудник выезжает к клиенту для осмотра места установки и составления сметы работ.
                </p>
            </div>
        </div>
        <div class="flwrap"></div>
        <div class="about__list--item">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-ventilation.png" alt="">
            <div class="about__list--title">
                <h1>ОПЫТ И НАДЕЖНОСТЬ</h1>
                <p>
                    На протяжении 4-х лет нашими сотрудниками было смонтировано сотни кондиционеров и другого оборудования. В домах, офисах, квартирах наших заказчиков стало теплее и уютнее.
                </p>
            </div>
        </div>
        <div class="about__list--item">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-ventilation.png" alt="">
            <div class="about__list--title">
                <h1>БОЛЬШОЙ ВЫБОР ОБОРУДОВАНИЯ</h1>
                <p>
                    Мы являемся официальными дилерами мировых производителей кондиционеров. Наши сотрудники помогут подобрать оптимальный вариант оборудования под Ваши потребности и бюджет.
                </p>
            </div>
        </div>
    </div>
</section>

<?php get_footer('about'); ?>