<?php 
/*
Template Name: Каталог
*/
?>

<?php get_header('catalog'); ?>

<section id="search" class="search">
        <div class="container">
            <div class="row">
                <a onclick="history.back()" class="arrow-back" style="cursor: pointer;">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/arrow-back.png" alt="back">
                    <p>Назад</p>
                </a>
            </div>
            <div class="row">
                <div class="d1">
                    <form>
                        <input type="text" placeholder="Найти товар...">
                        <button type="submit">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/search-icon.png" alt="Search">
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </section>

<main class="catalog">
        <div class="container">
            <div class="row">
                <div class="col-7">
                    <div class="catalog--title">
                        <h1>Настенные сплит-системы</h1>
                    </div>
                </div>
                <div class="col-5">
                    <div class="sort">
                        <span class="sort--title">Сортировать:</span>
                        <div class="expensive" id="expensive">
                            <span>Сначала дорогие</span>
                        </div>
                        <div class="cheap btn-active" id="cheap">
                            <span>Сначала дешевые</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper">
            <section class="filter" id="filter">
                <div class="filter__wrapper">
                    <form action="#">
                        <div class="brend-btn mb40" id="brend-btn">
                            <p>Производитель <i></i></p>
                            <div class="brend-list" id="brend-list" style="display: none;">
                                <ul id="brend-list-ul"></ul>
                            </div>
                        </div>
                        <div class="price mb40">
                            <p>Цена</p>
                            <div class="price--choice">
                                <input type="text" placeholder="0">
                                <span>-</span>
                                <input type="text" placeholder="419 600">
                            </div>
                        </div>
                        <div class="invertor mb40">
                            <p>Инвертер</p>
                            <div class="invertor-block">
                                <div class="invertor-block-1">
                                    <input name="invertor" type="radio" id="yes" checked>
                                    <label for="yes">
                                        <p>Есть</p>
                                    </label>
                                </div>
                                <div class="invertor-block-2">
                                    <input name="invertor" type="radio" id="no">
                                    <label for="no">
                                        <p>Нет</p>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="work-time mb40">
                            <p>Инвертер</p>
                            <div class="work-time-block">
                                <div class="work-time-block-1">
                                    <input name="work-time" type="radio" id="one" checked>
                                    <label for="one">
                                        <p>Охлаждение и обогрев</p>
                                    </label>
                                </div>
                                <div class="inverter-block-2">
                                    <input name="work-time" type="radio" id="two">
                                    <label for="two">
                                        <p>Только охлаждение</p>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="stock mb40">
                            <p>Наличие</p>
                            <div class="stock-block">
                                <div class="stock-block-1">
                                    <input name="stock" type="radio" id="inStock" checked>
                                    <label for="inStock">
                                        <p>В наличие</p>
                                    </label>
                                </div>
                                <div class="stock-block-2">
                                    <input name="stock" type="radio" id="notStock">
                                    <label for="notStock">
                                        <p>На заказ</p>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="square mb40">
                            <p>Площадь помещения</p>
                            <div class="square--choice">
                                <input type="text" placeholder="от">
                                <span>-</span>
                                <input type="text" placeholder="до">
                            </div>
                        </div>
                        <div class="power mb40">
                            <p>Мощность</p>
                            <div class="power--choice">
                                <input type="text" placeholder="2,5">
                                <span>-</span>
                                <input type="text" placeholder="18">
                            </div>
                        </div>
                        <div class="guarantee mb40">
                            <p>Гарантия</p>
                            <div class="guarantee-block">
                                <div class="guarantee-block-1">
                                    <input name="guarantee" type="radio" id="1year" checked>
                                    <label for="1year">
                                        <p>1 год</p>
                                    </label>
                                </div>
                                <div class="guarantee-block-7">
                                    <input name="guarantee" type="radio" id="4year">
                                    <label for="4year">
                                        <p>4 года</p>
                                    </label>
                                </div>
                                <div class="guarantee-block-2">
                                    <input name="guarantee" type="radio" id="1-5year">
                                    <label for="1-5year">
                                        <p>1.5 года</p>
                                    </label>
                                </div>
                                <div class="guarantee-block-3">
                                    <input name="guarantee" type="radio" id="5year">
                                    <label for="5year">
                                        <p>5 лет</p>
                                    </label>
                                </div>
                                <div class="guarantee-block-4">
                                    <input name="guarantee" type="radio" id="2year">
                                    <label for="2year">
                                        <p>2 года</p>
                                    </label>
                                </div>
                                <div class="guarantee-block-5">
                                    <input name="guarantee" type="radio" id="7year">
                                    <label for="7year">
                                        <p>7 лет</p>
                                    </label>
                                </div>
                                <div class="guarantee-block-6">
                                    <input name="guarantee" type="radio" id="3year">
                                    <label for="3year">
                                        <p>3 года</p>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="submit-btn">
                            <div class="button"><span>Применить</span></div>
                            <div class="reset"><span>Сбросить фильтр</span></div>
                        </div>
                    </form>
                </div>
            </section>
            <section id="catalog-block" class="catalog-block">
                <?php getCards(16, stock); ?>
                <div class="more-btn">
                    <div class="more-icon"></div>
                    <p>Показать больше товаров</p>
                </div>
            </section>
        </div>
    </main>

<?php get_footer('catalog'); ?>