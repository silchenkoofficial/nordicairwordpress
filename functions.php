<?php

add_action( "wp_enqueue_scripts", "style_theme" );
add_action( "wp_footer", "scripts_theme" );

function style_theme() {
	wp_enqueue_style("style", get_stylesheet_uri());
	wp_enqueue_style( "bootstrap", get_template_directory_uri() . "/assets/css/bootstrap/bootstrap.css");
}

function scripts_theme() {
	wp_enqueue_script("main", get_template_directory_uri() . "/assets/js/main.js");
}

function getCards($value, $inStock) {
	echo "<div class=\"catalog__cards\">";
	for ($i=0; $i < $value; $i++) { 
		echo "<div class=\"catalog__cards--card\">";
		echo "<div class=\"catalog__cards--title\">";
		echo "<div class=\"img\"></div>";
		echo "<p class=\"title\">Кондиционер 1</p>";
		echo "<p class=\"price\">10 000 ₽</p>";
		echo "<span class=\"availability {$inStock}\">";
		echo "<div class=\"item-{$inStock}\"></div>";
		echo "<p>В наличии</p>";
		echo "</span>";
		echo "<a href=\"page-product\">Подробнее</a>";
		echo "</div>";
		echo "</div>";
	}
	echo "</div>";
}

add_theme_support('custom-logo');