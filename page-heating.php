<?php 
/*
Template Name: Отопление
*/
?>

<?php get_header('heating'); ?>

    <section id="search" class="search">
        <div class="container">
            <div class="row">
                <a onclick="history.back()" class="arrow-back" style="cursor: pointer;">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/arrow-back.png" alt="back">
                    <p>Назад</p>
                </a>
            </div>
            <div class="row">
                <div class="d1">
                    <form>
                        <input type="text" placeholder="Найти товар...">
                        <button type="submit">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/search-icon.png" alt="Search">
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </section>


    <div class="heating__title">
        <h1>Отопление</h1>
    </div>
    <section id="heating" class="heating">
        <div class="container">
            <div class="heating__blocks">
                <div class="heating__blocks--1">
                    <h1>Тепловые завесы</h1>
                    <div class="ventilation__block">
                        <div class="ventilation__block--card card-1">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/heating-1.png" alt="">
                            <div class="ventilation__block--card--title">
                                <div class="ventilation__block--card--title--desc">
                                    <h2>Водяные</h2>
                                    <p>
                                        Самые мощные приборы в линейке высоконапорных завес, основным источником тепловой энергии которых является горячая вода.
                                    </p>
                                </div>
                                <a href="catalog">В каталог</a>
                            </div>
                        </div>
                        <div class="ventilation__block--card card-2">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/heating-2.png" alt="">
                            <div class="ventilation__block--card--title">
                                <div class="ventilation__block--card--title--desc">
                                    <h2>Электрические</h2>
                                    <p>
                                        Создадут невидимый барьер воздушного потока. В нашем каталоге только самые эффективные и энергосберегающие модели!
                                    </p>
                                </div>
                                <a href="catalog">В каталог</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="heating-lines"></div>
                <div class="heating-add"></div>
                <div class="heating__blocks--2">
                    <h1>Тепловые насосы</h1>
                    <div class="ventilation__block">
                        <div class="ventilation__block--card card-1">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/heating-3.png" alt="">
                            <div class="ventilation__block--card--title">
                                <div class="ventilation__block--card--title--desc">
                                    <h2>Воздух-воздух</h2>
                                    <p>
                                        Обеспечивают значительную экономию расходов на обогрев. Теплонасосы способны как охлаждать, так и нагревать воздушные массы в доме.
                                    </p>
                                </div>
                                <a href="catalog">В каталог</a>
                            </div>
                        </div>
                        <div class="ventilation__block--card card-2">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/heating-4.png" alt="">
                            <div class="ventilation__block--card--title">
                                <div class="ventilation__block--card--title--desc">
                                    <h2>Воздух-вода</h2>
                                    <p>
                                        Идеальное использование для небольших помещений, а также простая установка не требующая скважин или дополнительной площади!
                                    </p>
                                </div>
                                <a href="catalog">В каталог</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php get_footer('heating'); ?>