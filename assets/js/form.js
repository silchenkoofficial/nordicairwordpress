let questionInput = document.getElementById('question-input');
let telephoneInput = document.getElementById('telephone-input');
let emailInput = document.getElementById('email-input');

let contactForm = document.getElementById('invisible-form');
let submitBtn = document.getElementById('form-submit-btn');
let sendBlock = document.getElementById('send-block');

submitBtn.onclick = () => {
    if (questionInput.value == '' | telephoneInput.value == '' | emailInput.value == '') {
        alert("Введите данные корректно!");
    }
    else {
        contactForm.style.display = 'none';
        sendBlock.style.display = 'block';
    }
}