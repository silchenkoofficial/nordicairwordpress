let brendBtn = document.getElementById('brend-btn');
let brendList = document.getElementById('brend-list');
let brendListUl = document.getElementById('brend-list-ul');

let expensiveBtn = document.getElementById('expensive');
let cheapBtn = document.getElementById('cheap');

let brendName = [
    'Aero', 'Aerolite', 'Aeronik', 'Airwell', 'Axioma',
    'Ballu',
    'Carrier', 'Chigo',
    'Daikin', 'Dahatsu',
    'Electrolux',
    'Fujitsu',
    'General', 'Gree',
    'Haier', 'Hitachi', 'Hisense',
    'IGC',
    'Kentatsu',
    'Lanzkraft', 'Leberg', 'Lessar', 'LG',
    'MDV', 'Midea', 'Mitsubishi Electric',
    'Panasonic',
    'Quattroclima',
    'Roda', 'Royal Clima',
    'Samsung',
    'TCL', 'Toshiba', 'Tosot',
    'Zanussi'
]

for (let i = 0; i < brendName.length; i++) {
    brendListUl.innerHTML += "<li><a href=\"#\">" + brendName[i] + "</a></li>";
}

// ------------------------------

expensiveBtn.onclick = () => {
    cheapBtn.classList.remove('btn-active');
    expensiveBtn.classList.add('btn-active');
}

cheapBtn.onclick = () => {
    expensiveBtn.classList.remove('btn-active');
    cheapBtn.classList.add('btn-active');
}

brendBtn.onclick = () => {
    if (brendList.style.display == 'none') {
        brendList.style.display = 'block';
    }
    else if (brendList.style.display == 'block') {
        brendList.style.display = 'none';
    }
}