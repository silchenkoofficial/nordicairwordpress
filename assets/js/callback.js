const
    header = document.querySelector('.header'),
    search = document.querySelector('.search'),
    services = document.querySelector('.services'),
    callback = document.querySelector('.callback'),
    footer = document.querySelector('.footer');

const
    inputName = document.querySelector('#inputName'),
    inputTel = document.querySelector('#inputTel'),
    inputTime = document.querySelector('#inputTime');

const
    callbackModal = callback.querySelector('.callback-modal'),
    callbackSend = callback.querySelector('.callback-send'),
    closeBtn = callback.querySelector('.callback-close-btn'),
    submitBtn = callback.querySelector('.callback-modal-btn');

const openCallbackWindow = () => {
    header.classList.add('callback_blur');
    search.classList.add('callback_blur');
    services.classList.add('callback_blur');
    footer.classList.add('callback_blur');

    callback.style.display = 'block';
}

const dataCheck = () => {
    if(inputName.value == '' | inputTel.value == '') {
        alert("Пожалуйста, введите все данные.");
    } else {
        callbackModal.style.display = 'none';
        callbackSend.style.display = 'flex';
    }
}

const closeCallbackWindow = () => {
    header.classList.remove('callback_blur');
    search.classList.remove('callback_blur');
    services.classList.remove('callback_blur');
    footer.classList.remove('callback_blur');

    callback.style.display = 'none';
}

