const catalog = document.getElementById('catalog');
const dropdownMenu = document.getElementById('dropdown--menu');
const closeWindow = document.querySelector('.close-window');

catalog.onclick = () => {
    if(!catalog.classList.contains('active')) {
        catalog.classList.add('active');
        dropdownMenu.style.display = 'block';
        closeWindow.style.display = 'block';
    }
    else {
        catalog.classList.remove('active');
        dropdownMenu.style.display = 'none';
        closeWindow.style.display = 'none';
    }
}

closeWindow.onclick = () => {
    catalog.classList.remove('active');
    dropdownMenu.style.display = 'none';
    closeWindow.style.display = 'none';
}