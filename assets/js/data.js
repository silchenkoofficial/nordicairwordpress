const 
    callbackBtn = document.querySelector('button[type="submit"]');

callbackBtn.addEventListener('click', () => {
    const
        name = document.querySelector('#inputName').value,
        telephone = document.querySelector('#inputTel').value,
        time = document.querySelector('#inputTime').value;
    console.log({name, telephone, time});
});