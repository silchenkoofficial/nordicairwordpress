const btnDescription = document.getElementById('button-description'),
      btnProperty = document.getElementById('button-property'),
      productDesc1 = document.querySelector('.product__description'),
      productDesc2 = document.querySelector('.product__description2');


btnDescription.onclick = () => {
    btnProperty.classList.remove('btn-active');
    productDesc2.style.display = 'none';
    btnDescription.classList.add('btn-active');
    productDesc1.style.display = 'block';
}

btnProperty.onclick = () => {
    btnDescription.classList.remove('btn-active');
    productDesc1.style.display = 'none';
    btnProperty.classList.add('btn-active');
    productDesc2.style.display = 'block';
}