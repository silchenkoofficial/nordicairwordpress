<?php 
/*
Template Name: Услуги
*/
?>

<?php get_header('services'); ?>

<section id="search" class="search">
    <div class="container">
        <div class="row">
            <a onclick="history.back()" class="arrow-back" style="cursor: pointer;">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/arrow-back.png" alt="back">
                <p>Назад</p>
            </a>
        </div>
    </div>
</section>

<section class="services" id="services">
    <div class="container">
        <h1>Наши услуги</h1>
    </div>
    <div class="services__main">
        <div class="services__title">
            <div class="wrapper">
                <div class="services__title--desc">
                    <p>
                        Наша организация оказывает весь спектр услуг
                        по системам вентиляции и кондиционирования воздуха,
                        а именно:
                    </p>
                    <ul>
                        <li><span>»</span> проектирование</li>
                        <li><span>»</span> монтаж</li>
                        <li><span>»</span> ремонт</li>
                        <li><span>»</span> сервисное обслуживание</li>
                        <li><span>»</span> гарантийный ремонт</li>
                        <li><span>»</span> послегарантийное обслуживание</li>
                    </ul>
                    <p>
                        С ценами Вы сможете ознакомиться после составления коммерческого предложения.
                    </p>
                </div>
            </div>
        </div>
        <div class="services__work">
            <div class="work-img">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/work-1.png" alt="" class="img-1">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/work-2.png" alt="" class="img-2">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/work-3.png" alt="" class="img-3">
            </div>
            <a href="javascript://">Посмотреть наши работы <span class="work-arrow"></span></a>
        </div>
    </div>
    <div class="services__callbackBtn" onclick="openCallbackWindow()">Заказать обратный звонок</div>
</section>

<div class="callback" style="display: none;">
    <div class="callback-modal">
        <div class="callback-close-btn" onclick="closeCallbackWindow()"></div>
        <h1 class="callback-modal-title">Заполните форму и мы перезвоним</h1>
        <form onsubmit="return false" class="callback-modal-form" method="POST">
            <div class="callback-modal-form-inputs">
                <input name="name" type="text" placeholder="Ваше имя*" require id="inputName">
                <input name="telephone" type="tel" placeholder="Ваш номер телефона*" require id="inputTel">
                <input name="time" type="text" placeholder="Удобное для Вас время звонка" require id="inputTime">
            </div>
            <button onclick="dataCheck()" type="submit" class="callback-modal-btn">Отправить</button>
        </form>
    </div>
    <div class="callback-send" style="display: none">
        <div class="callback-close-btn" onclick="closeCallbackWindow()"></div>
        <h1>Ваше сообщение отправлено!</h1>
    </div>
</div>

<?php get_footer('services'); ?>
