<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package nordicAir
 */

get_header(); ?>

<div class="error-page" style="
	display: flex;
	-webkit-flex-direction: column;
	    -ms-flex-direction: column;
	        flex-direction: column;
	-webkit-align-items: center;
	        align-items: center;
	-webkit-justify-content: center;
	        justify-content: center;
	height: 550px;
">
	<h1>Упс... Такой страницы еще нет:(</h1>
	<a href="<?php bloginfo('url'); ?>">Вернитесь на главную страницу</a>
</div>
	
<?php get_footer(); ?>
