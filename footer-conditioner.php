	<footer id="footer" class="footer">
        <div class="container">
            <div class="row">
                <div class="col-3">
                    <div class="footer__title">
                        <div class="footer__logo">
                            <a class="header-link" href="<?php bloginfo("url"); ?>">NordicAir</a>
                            <a class="p-link" href="javascript://">Политика конфидециальности</a>
                        </div>
                        <div class="footer__copyright">
                            <p>© NordicAir 2020</p>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="footer__menu">
                        <ul>
                            <li><a href="catalog">Каталог</a></li>
                            <li><a href="services">Услуги</a></li>
                            <li><a href="about">О нас</a></li>
                        </ul>
                        <ul>
                            <li><a href="javascript://">Контакты</a></li>
                            <li><a href="javascript://">Наши работы</a></li>
                            <li><a href="javascript://">Партнерам</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-3">
                    <div class="footer__icons">
                        <a href="javascript://" class="whatsapp-icon"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/whatsapp.png" alt="Whatsapp"></a>
                        <a href="javascript://" class="viber-icon"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/viber.png" alt="Viber"></a>
                        <a href="javascript://" class="instagram-icon"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/instagram.png" alt="Instagram"></a>
                    </div>
                </div>
            </div>
        </div>
    </footer>

	<?php wp_footer(); ?>

</body>
</html>