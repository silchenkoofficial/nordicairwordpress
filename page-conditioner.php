<?php 
/*
Template Name: Кондиционирование
*/
?>

<?php get_header('conditioner'); ?>

    <section id="search" class="search">
        <div class="container">
            <div class="row">
                <a onclick="history.back()" class="arrow-back" style="cursor: pointer;">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/arrow-back.png" alt="back">
                    <p>Назад</p>
                </a>
            </div>
            <div class="row">
                <div class="d1">
                    <form>
                        <input type="text" placeholder="Найти товар...">
                        <button type="submit">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/search-icon.png" alt="Search">
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section id="condition" class="condition" style="height: 601px;">
        <div class="container">
            <div class="condition__title">
                <h1>Кондиционирование</h1>
            </div>
            <div class="ventilation__block">
                <div class="ventilation__block--card card-1">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/condition-card-1.png" alt="">
                    <div class="ventilation__block--card--title">
                        <div class="ventilation__block--card--title--desc">
                            <h2>Настенные сплит-системы</h2>
                            <p>
                                Настенные кондиционеры для дома, квартиры, офиса или магазина. В данном каталоге представлены только лучшие решения!
                            </p>
                        </div>
                        <a href="catalog">В каталог</a>
                    </div>
                </div>
                <div class="ventilation__block--card card-2">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/condition-card-2.png" alt="">
                    <div class="ventilation__block--card--title">
                        <div class="ventilation__block--card--title--desc">
                            <h2>Мульти сплит-системы</h2>
                            <p>
                                Требуется установить несколько систем в одном помещении? Тогда такое решение незаменимо для вас!  Удобно. Качественно. Надёжно.
                            </p>
                        </div>
                        <a href="catalog">В каталог</a>
                    </div>
                </div>
                <div class="ventilation__block--card card-3">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/condition-card-3.png" alt="">
                    <div class="ventilation__block--card--title">
                        <div class="ventilation__block--card--title--desc">
                            <h2>Полупромышленные сплит-системы</h2>
                            <p>
                                От охлаждения серверных помещений до создания комфортной системы кондиционирования офисного здания!
                            </p>
                        </div>
                        <a href="catalog">В каталог</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php get_footer('conditioner'); ?>